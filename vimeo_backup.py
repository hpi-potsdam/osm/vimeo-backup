#!/usr/bin/env python3
"""
This scripts downloads all videos of a Vimeo account. It selects the
largest file for download per video and skips existing files if their
hash matches the file that would be downloaded.
See also https://gitlab.com/hpi-potsdam/osm/vimeo-backup , especially
on how to get the information for the configuration file.
"""

# this is more a CLI script than a module, allow dash in file name:
# pylint: disable=invalid-name

import sys
import argparse

from configparser import ConfigParser
from hashlib import md5
from logging import getLogger, INFO, WARN, info, warning, error
from multiprocessing import (Process, JoinableQueue, BoundedSemaphore,
                             Value)
from os import chdir, cpu_count
from os.path import isfile, getsize
from socket import gaierror
from time import sleep
from traceback import format_exc

import requests
from vimeo import VimeoClient



# block size for chunked IO
CHUNK_SIZE = 2048

# retry after these amounts of seconds
REQUESTS_RETRIES = (0, 1, 3)



def setup_logging(verbose_enabled):
  '''
  configures our logger, incl. setting the log level according to
  :arg:`verbose_enabled`
  '''
  logger = getLogger()
  logger.name = ''
  if verbose_enabled:
    logger.setLevel(INFO)
  else:
    logger.setLevel(WARN)



def get_cli_args():
  '''
  builds CLI, returns parsed arguments as dict
  '''
  info('setting up CLI')

  def check_positive(value):
    number = int(value)
    if number <= 0:
      raise argparse.ArgumentTypeError('must be greater zero')
    return number

  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
  )

  parser.add_argument('-o', '--out-dir', default='.',
                      help='directory to back up to')
  parser.add_argument('-j', '--parallel', type=check_positive,
                      default=cpu_count(),
                      help='processes to use (mainly for hashing)')
  parser.add_argument('-v', '--verbose', action='store_true',
                      default=False, help='turn on verbose messages')
  parser.add_argument('config_file', type=str,
                      help=('configuration file to load (must contain '
                            '"access-token = …", "client-id = …" '
                            'and "client-secret = …")'))

  return vars(parser.parse_args())



def get_ini_args(file_name):
  '''
  reads config file, returns given options as dict
  '''
  encodings = ('utf-8', 'utf-16', 'windows-1250', 'windows-1252')
  for encoding in encodings:
    config_parser = ConfigParser()
    try:
      with open(file_name, encoding=encoding) as config_file:
        config_parser.read_string('[default]\n' + config_file.read())
    except UnicodeDecodeError:
      continue
    else:
      break
  else:
    error(f'could not open config file with {", ".join(encodings)} '
          'encoding')
    sys.exit(1)

  out = config_parser['default']

  for required_option in ('access-token', 'client-id', 'client-secret'):
    if required_option not in out:
      error(f'please provide option "{required_option}" '
            '(via config file)')
      sys.exit(1)

  return out


def pprint_call(func, args, kwargs):
  '''
  returns pretty-printed string for ``func`` with ``args`` and
  ``kwargs``
  '''
  out = func.__name__
  out += '('
  if args:
    out += ', '.join(args)
  if kwargs:
    out += ', '.join(f'{k}={v}' for k, v in kwargs.items())
  out += ')'
  return out


def retry_requests(func, *args, **kwargs):
  '''
  convenience wrapper to retry requests when corresponding exceptions
  are raised
  '''
  retries = iter(REQUESTS_RETRIES)
  while True:
    try:
      return func(*args, **kwargs)
    except (requests.exceptions.RequestException, gaierror) \
        as request_exception:
      try:
        warning(f'request {pprint_call(func, args, kwargs)} failed, '
                'will retry')
        sleep(next(retries))
      except StopIteration:
        error(f'request {pprint_call(func, args, kwargs)} failed '
              f'{len(REQUESTS_RETRIES)} times; giving up')
        raise request_exception from None


def enqueue_videos(client, queue):
  '''
  queries videos using :args:`client`, and submits (all of) them into
  :args:`queue`
  '''
  info('getting list of videos to download')

  page_urls = ['/me/videos']
  while page_urls:

    page_url = page_urls.pop(0)

    if page_url is None:
      break

    info(f'requesting {page_url}')
    page = retry_requests(client.get, page_url).json()

    page_urls.append(page['paging']['next'])

    for video in page['data']:
      info(f'enqueue "{video["name"]}"')
      queue.put(video)

  info('all videos enqueued')



def get_largest_video_file(video):
  '''
  takes a :arg:`video` (as returned by the Vimeo API), returns the
  "video file" (as returned by the Vimeo API) of the largest file size
  '''
  out = None
  for candidate in video['files']:
    if out is None or candidate['size'] > out.get('size', -1):
      out = candidate
  return out



def get_file_name(video, video_file):
  '''
  takes a :arg:`video` and a :arg:`video_file` (both as returned by the
  Vimeo API) and returns a string suitable as a corresponding local file
  name
  '''

  out = (
    f'{video["name"]}'
    f' - {video["uri"].replace("/videos/", "")}'
    f'.{video_file["type"].split("/")[-1]}'
  )

  # avoid that files end up hidden; also catches '.' and '..'
  if out.startswith('.'):
    out = '_' + out

  out = out.replace('/', '_') # thanks but no thanks: slashes
  out = out.replace('\0', '_') # e.g., ext4 does not allow null bytes

  return out



def download(link, dest_file_name):
  '''
  actually downloads file :arg:`link` to :arg:`dest_file_name`
  unconditionally; returns md5 of downloaded data
  '''
  request = requests.get(link, stream=True, timeout=60)
  hash_ = md5()
  with open(dest_file_name, 'wb') as dest_fp:
    for chunk in request.iter_content(chunk_size=CHUNK_SIZE):
      if chunk:
        dest_fp.write(chunk)
        hash_.update(chunk)
  return hash_



def needs_download(video_file, dest_file_name):
  '''
  checks if :arg:`video_file` (as returned by Vimeo API) needs to be
  downloaded to :arg:`dest_file_name`
  '''
  if not isfile(dest_file_name):
    info(f'file no yet downloaded: "{dest_file_name}"')
    return True

  # comparing by size is inaccurate, compare with 1 % tolerance
  assert video_file['size'] not in [0, None]
  if abs(1 - getsize(dest_file_name) / video_file['size']) > .01:
    info(f'size of downloaded file differs: "{dest_file_name}"')
    return True

  # according to Vimeo support team, the MD5 might not be returned
  if video_file['md5'] is None:
    info(f'Vimeo does not provide a hash for "{dest_file_name}"')
  else:
    info(f'hashing "{dest_file_name}"')
    dest_file_hash = md5()
    with open(dest_file_name, 'rb') as dest_file:
      for chunk in iter(lambda: dest_file.read(CHUNK_SIZE), b''):
        dest_file_hash.update(chunk)
    if dest_file_hash.hexdigest() == video_file['md5']:
      info(f'hash matches: "{dest_file_name}"')
      return False
    info(f'hash mismatch: "{dest_file_name}"')

  # if we cannot say we have the correct video on disk, download:
  return True



def process_video(video, download_sema):
  '''
  main coordination for the backup of a single :arg:`video` (as
  returned by the Vimeo API)
  '''
  video_file = get_largest_video_file(video)
  if video_file is None:
    print(f'skipping "{video["name"]}": no video file '
          '(incomplete upload?)')
    return True

  dest_file_name = get_file_name(video, video_file)

  if not needs_download(video_file, dest_file_name):
    print(f'skipping "{dest_file_name}": up-to-date')
    return True

  with download_sema:
    print(f'downloading "{dest_file_name}"')
    download_hash = retry_requests(download, video_file['link'],
                                   dest_file_name)
  if (video_file['md5'] is not None and
      download_hash.hexdigest() != video_file['md5']):
    error(f'hash mismatch after download: "{dest_file_name}"')
    return False

  return True



def dequeue_videos(video_queue, download_sema, error_count):
  '''
  out-most loop for sub processes that download enqueued videos: take
  videos out of the queue; initiate their backup process; handle errors
  and unhandled exceptions, if any
  '''
  while True:
    video = video_queue.get()
    info(f'dequeue "{video["name"]}"')
    try:
      success = process_video(video, download_sema)
      assert isinstance(success, bool)

    # we *really* want to keep going and not crash a download process:
    # pylint: disable=broad-except
    except Exception as exception:
      error(f'unhandled exception: {exception}')
      info(format_exc())
      success = False

    if not success:
      with error_count.get_lock():
        error_count.value += 1
    video_queue.task_done()



def main():
  '''
  main procedure, containing top-level coordination
  '''
  cli_args = get_cli_args()
  ini_args = get_ini_args(cli_args['config_file'])

  setup_logging(cli_args['verbose'])

  chdir(cli_args['out_dir'])

  client = VimeoClient(
    token=ini_args['access-token'],
    key=ini_args['client-id'],
    secret=ini_args['client-secret'],
  )

  video_queue = JoinableQueue(cli_args['parallel'] * 2)
  download_sema = BoundedSemaphore(2)
  error_count = Value('i', 0)

  dowload_processes = [
    Process(target=dequeue_videos,
            args=(video_queue, download_sema, error_count))
    for _ in range(cli_args['parallel'])
  ]

  for process in dowload_processes:
    process.start()

  try:
    # needs to be synchronous, so we know when no more videos will be
    # enqueued
    enqueue_videos(client, video_queue)
  except KeyboardInterrupt:
    info('user interrupted execution – terminating')
  else:
    video_queue.join()
  finally:
    for process in dowload_processes:
      process.terminate()

  with error_count.get_lock():
    if error_count.value > 0:
      sys.exit(min(2 + error_count.value,  255))



if __name__ == '__main__':
  main()
