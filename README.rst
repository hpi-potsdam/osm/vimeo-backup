.. image:: https://gitlab.com/hpi-potsdam/osm/vimeo-backup/badges/main/pipeline.svg
  :target: https://gitlab.com/hpi-potsdam/osm/vimeo-backup/pipelines
  :align: right

============
Vimeo backup
============

A script to download all videos of a Vimeo account.

It downloads the file of largest size per (Vimeo) video and skips
already downloaded files based on their hash.

A Vimeo API access token, a client ID and a client secret is required.
Those can be obtained by following the `Vimeo authentication process
<https://developer.vimeo.com/api/authentication>`__.

quick start
===========

.. code:: shell

  # create virtualenv/pipenv to your liking

  # installation:
  pip3 install --user git+https://gitlab.com/hpi-potsdam/osm/vimeo-backup.git

  # example usage:

  # create and enter directory for backups:
  mkdir ~/my-vimeo-backup
  cd ~/my-vimeo-backup

  # create a configuration file:
  cat << EOF > vimeo-backup.ini
  access-token = <set your access token here>
  client-id = <set your client id here>
  client-secret = <set your client secret here>
  EOF

  # backup videos:
  vimeo-backup vimeo-backup.ini

Or, if you prefer to clone this repository:

.. code:: shell

  git clone https://gitlab.com/hpi-potsdam/osm/vimeo-backup.git

  # create virtualenv/pipenv to your liking

  # install dependencies:
  pip3 install -e vimeo-backup

  # create configuration file:
  cp vimeo-backup/example-config.ini vimeo-backup.ini
  nano vimeo-backup.ini

  # create output directory:
  mkdir videos

  # backup videos:
  vimeo-backup/vimeo_backup.py -o videos vimeo-backup.ini

exit codes
==========

:0: on success
:1: for errors in the configuration file
:2: for errors regarding the CLI arguments
:2 + <number of download errors occurred>: else
