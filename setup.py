from os.path import join as path_join, dirname
from setuptools import setup

version = '0.1'
README = path_join(dirname(__file__), 'README.rst')
long_description = open(README).read()
setup(
    name='vimeo-backup',
    version=version,
    description=('downloads all videos of a Vimeo account'),
    long_description=long_description,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python',
    ],
    author='Lukas Pirl',
    author_email='vimeo-backup@lukas-pirl.de',
    url='https://gitlab.com/hpi-potsdam/osm/vimeo-backup/-/archive/master/vimeo-backup-master.tar.bz2',
    install_requires=[
        'aiohttp==3.9.0b0', # https://github.com/aio-libs/aiohttp/issues/7739
        'PyVimeo',
    ],
    extras_require={
        'dev': [
            'pylint',
        ],
    },
    py_modules=['vimeo_backup'],
    entry_points = {
        'console_scripts': ['vimeo-backup=vimeo_backup:main'],
    },
)
